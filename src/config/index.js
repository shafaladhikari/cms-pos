import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {BASE_URL} from './baseUrl';

const getToken = async () => {
  const token = await AsyncStorage.getItem('accessToken');
  // const parseToken = JSON.parse(token);
  return 'Bearer' + ' ' + token;
};

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});
axiosInstance.interceptors.request.use(async function (config) {
  const token = await getToken();
  config.headers.Authorization = token;
  return config;
});
export default axiosInstance;
